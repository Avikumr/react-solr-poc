package com.react.solr.reactsolrapi.model;

import lombok.Builder;
import lombok.Data;
import org.apache.solr.client.solrj.beans.Field;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Data
@Builder
//@SolrDocument(solrCoreName = "productapi")
public class Product {

    @Column(name= "id")
    @Field
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @Column(name= "name")
    @Field
    private String name;

    @Column(name= "brandName")
    @Field
    private String brandName;

    @Column(name= "productType")
    @Field
    private String productType;

    @Column(name= "price")
    @Field
    private Double price;

    @Column(name= "size")
    @Field
    private Long size;

    @Column(name= "productColor")
    @Field
    private String productColor;

    public Product(String id, String name, String brandName, String productType, Double price, Long size, String productColor) {
        this.id = id;
        this.name = name;
        this.brandName = brandName;
        this.productType = productType;
        this.price = price;
        this.size = size;
        this.productColor = productColor;
    }

    public Product(){

    }

}
