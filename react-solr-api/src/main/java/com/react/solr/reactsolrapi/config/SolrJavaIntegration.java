package com.react.solr.reactsolrapi.config;

import java.io.IOException;

import com.react.solr.reactsolrapi.model.Product;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.impl.XMLResponseParser;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrException;
import org.apache.solr.common.SolrInputDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class SolrJavaIntegration {

    protected static final Logger logger = LoggerFactory.getLogger(SolrJavaIntegration.class);

    private HttpSolrClient solrClient;

    public SolrJavaIntegration(String clientUrl) {

        solrClient = new HttpSolrClient.Builder(clientUrl).build();
        solrClient.setParser(new XMLResponseParser());
    }

    public SolrJavaIntegration() {

        solrClient = new HttpSolrClient.Builder("http://localhost:8983/solr/productapi").build();
        solrClient.setParser(new XMLResponseParser());
    }

    public void addProductBean(Product pBean) throws IOException, SolrServerException {

        solrClient.addBean(pBean);
        solrClient.commit();
    }

    public void addSolrDocument(String documentId, String itemName, String itemPrice) throws SolrServerException, IOException {

        SolrInputDocument document = new SolrInputDocument();
        document.addField("id", documentId);
        document.addField("name", itemName);
        document.addField("price", itemPrice);
        solrClient.add(document);
        solrClient.commit();
    }

    public void addSolrDocument(String documentId, Product product) throws SolrServerException, IOException {

        SolrInputDocument document = new SolrInputDocument();
        document.addField("id", documentId);
        document.addField("name", product.getId());
        document.addField("brandName", product.getBrandName());
        document.addField("productType", product.getProductType());
        document.addField("price", product.getPrice());
        document.addField("size", product.getSize());
        document.addField("productColor", product.getProductColor());
        solrClient.add(document);
        solrClient.commit();
    }

    public void deleteSolrDocumentById(String documentId) throws SolrServerException, IOException {

        solrClient.deleteById(documentId);
        solrClient.commit();
    }

    public void deleteSolrDocumentByQuery(String query) throws SolrServerException, IOException {

        solrClient.deleteByQuery(query);
        solrClient.commit();
    }

    public HttpSolrClient getSolrClient() {
        return solrClient;
    }

    protected void setSolrClient(HttpSolrClient solrClient) {
        this.solrClient = solrClient;
    }

    protected QueryResponse fireQuery(SolrQuery query) {
        try {
            logger.debug("Executing query: {}", query);
            final QueryResponse response = solrClient.query(query);
            logger.trace("Response: {}", response);
            return response;
        } catch(SolrException | IOException e) {
            logger.error("Error getting data:", e);
            throw new RuntimeException(e);
        } catch (SolrServerException e) {
            logger.error("Error getting data:", e);
            throw new RuntimeException(e);
        }
    }

    /**
     * Basic sanitising of Solr queries.
     *
     * TODO: Move this to QueryFacetSelection level??
     *
     * Query is based on the URL to the VLO web application. Also, explain about
     * the URL and ?fq=language:dutch Assume filters have the form a:b like for
     * example language:dutch
     *
     * @param query
     * @return
     */
    protected SolrQuery sanitise(SolrQuery query) {

//        // String [] facetsFromConfig;
//        // try and get the filters facets from the query
//        String[] filtersInQuery;
//        filtersInQuery = query.getFilterQueries();
//
//        if (filtersInQuery == null) {
//            // the query does not contain filters
//        } else {
//            // get the facets from the configuration file
//            // facetsFromConfig = VloConfig.getFacetFields();
//
//            // check the filters in the query by name
//            for (String filter : filtersInQuery) {
//                // split up a filter, look at the string preceeding the semicolon
//                String facetInFilter = filter.split(":")[0]
//                        .replaceFirst("^\\{!tag=.*\\}", ""); //strip out tag syntax (may be used for OR queries, see query factory implementation)
//
//                if (FacetConstants.AVAILABLE_FACETS.contains(facetInFilter)) {
//                    // facet in the filter is in the set that is defined by the config file
//                } else {
//                    if (facetInFilter.startsWith("_")) {
//                        // this facet is hidden, do not consider it
//                    } else {
//                        // the filter name does not match a facet in the facet
//                        query.removeFilterQuery(filter);
//                    }
//                }
//            }
//        }

        // finally, return the sanitised query
        return query;
    }

}
