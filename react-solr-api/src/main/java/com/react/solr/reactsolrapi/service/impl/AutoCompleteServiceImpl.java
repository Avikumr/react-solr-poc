package com.react.solr.reactsolrapi.service.impl;

import com.react.solr.reactsolrapi.config.SolrJavaIntegration;
import com.react.solr.reactsolrapi.service.AutoCompleteService;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.SpellCheckResponse;
import org.apache.solr.client.solrj.response.Suggestion;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class AutoCompleteServiceImpl extends SolrJavaIntegration implements AutoCompleteService {

    /**
     * Returns list of suggestions for incomplete input (used for autocomplete
     * function)
     *
     * @param input user input
     * @return iterator over suggestions
     */
    @Override
    public Iterator<String> getChoices(String input) {
        if (input != null) {

            final SolrQuery query = new SolrQuery();
            query.setQuery(input.toLowerCase());
            query.setRequestHandler("/suggest");

            final QueryResponse response = fireQuery(sanitise(query));

            if (response.getSpellCheckResponse() != null) {
                final List<SpellCheckResponse.Suggestion> suggestions = response.getSpellCheckResponse().getSuggestions();
                if (suggestions.size() > 0) {
                    return suggestions.get(0).getAlternatives().iterator();
                }
            }
        }

        return  Collections.emptyIterator();
    }
}
