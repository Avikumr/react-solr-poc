package com.react.solr.reactsolrapi.service;

import java.util.Iterator;

public interface AutoCompleteService {

    /**
     * Returns list of suggestions for incomplete input (used for autocomplete
     * function)
     *
     * @param input user input
     * @return iterator over suggestions
     */
    Iterator<String> getChoices(String input);

}
