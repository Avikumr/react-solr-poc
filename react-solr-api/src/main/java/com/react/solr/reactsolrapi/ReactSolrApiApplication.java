package com.react.solr.reactsolrapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories(basePackages = "com.react.solr.reactsolrapi.repository")
@SpringBootApplication
@ComponentScan({"com.react.solr.reactsolrapi.resource"})
@EntityScan("com.react.solr.reactsolrapi.model")
public class ReactSolrApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReactSolrApiApplication.class, args);
	}
}
