package com.react.solr.reactsolrapi.resource;

import com.react.solr.reactsolrapi.config.SolrJavaIntegration;
import com.react.solr.reactsolrapi.model.Product;
import com.react.solr.reactsolrapi.repository.ProductRepository;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.beans.DocumentObjectBinder;
import org.apache.solr.common.SolrDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.stream.LongStream;

@RestController
@CrossOrigin
@RequestMapping(value = "/rest/products")
@ComponentScan(basePackages = "com.react.solr.reactsolrapi.repository")
public class ProductResource {

    @Value("${solrURL}")
    String solrUrl;

    @Autowired
    ProductRepository productRepository;

    private SolrJavaIntegration solrJavaIntegration = new SolrJavaIntegration();

    @GetMapping(value = "/all")
    public Page<Product> getAll(){
        return productRepository.findAll(new PageRequest(0,100000d));
    }

    @PostMapping(value = "/load")
    public Optional<Product> persist(@RequestBody final Product product) throws IOException, SolrServerException {
        System.out.println("Product bean : "+product);
        productRepository.save(product);
        System.out.println(productRepository.findById(product.getId()));
        solrJavaIntegration.addProductBean(product);
        return productRepository.findById(product.getId().toString());
    }

    @GetMapping(value = "get/{id}")
    public Product getProductById(@PathVariable("id") String id ) throws IOException, SolrServerException {
        SolrDocument doc = solrJavaIntegration.getSolrClient().getById(id);
        System.out.println("solr Document : "+doc.getFieldNames());
        System.out.println("solr Document : "+doc.getFieldValuesMap());

        DocumentObjectBinder binder = new DocumentObjectBinder();
        Product product = binder.getBean(Product.class, doc);
        return product;
    }

    @GetMapping(value = "/insert")
    public void insertRecords(){
        LongStream.range(0L, 1000000).parallel().forEach( (value) ->{
                    double min = 1000.00;
                    double max = 10000.00;
                    Product product = Product.builder()
                            .name("Test " +value)
                            .brandName(value+" Desc")
                            .price((double) value)
                            .size(9L)
                            .productType(value + " Type")
                            .productColor("black")
                            .build();
                    productRepository.save(product);
                    try {
                        solrJavaIntegration.addProductBean(product);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (SolrServerException e) {
                        e.printStackTrace();
                    }
                }
        );
    }

    @PostMapping(value = "/solrsearch/{searchQuery}")
    public List<Product> searchSolr(@PathVariable("searchQuery") String searchQuery){
        RestTemplate restTemplate = new RestTemplate();
        System.out.println("query : "+searchQuery);
        System.out.println("solr url : "+solrUrl+searchQuery);
        List<Product> product = (List<Product>) restTemplate.getForObject( solrUrl+searchQuery, List.class);
        return product;
    }
}
