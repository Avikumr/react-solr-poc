package com.react.solr.reactsolrapi;

import com.react.solr.reactsolrapi.model.Product;
import com.react.solr.reactsolrapi.resource.ProductResource;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.solr.client.solrj.SolrServerException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.concurrent.ThreadLocalRandom;

@RunWith(SpringRunner.class)
@DataJpaTest
@SpringBootTest
@ComponentScan(basePackages = "com.react.solr.reactsolrapi.*")
@EnableAutoConfiguration(exclude={DataSourceAutoConfiguration.class})
public class InsertRecordsTest {

    @Autowired
    ProductResource productResource;

//    @Test
//    public void insertRecords() throws IOException, SolrServerException {
//
//    }

}
