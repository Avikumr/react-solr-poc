import React, { Component } from 'react';
import { Col, Button, Form, FormGroup, Label, Input } from 'reactstrap';
import axios from 'axios';

export default class Create extends Component {
 constructor(props) {
        super(props);
        this.onChangeHostName = this.onChangeHostName.bind(this);
        this.onChangePort = this.onChangePort.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

        this.state = {
            name: '',
            brandName: '',
            productType:'',
            price:'',
            size:'',
            productColor:''
        }
    }
    onChangeHostName(e) {
        this.setState({
            [e.target.name]: e.target.value
        });
    }
    onChangePort(e) {
        this.setState({
            port: e.target.value
        });
    }
    onSubmit(e) {
        e.preventDefault();
        const product = {
            name: this.state.name,
            brandName: this.state.brandName,
            productType:this.state.productType,
            price:this.state.price,
            size:this.state.size,
            productColor:this.state.productColor
        }
        console.log(" Product being generated from front end  : "+ JSON.stringify(product));
        axios.post('http://localhost:8080/rest/products/load', product)
        .then(res => console.log(res.data));

        this.setState({
            name: '',
            brandName: '',
            productType:'',
            price:'',
            size:'',
            productColor:''
        });
    }

    render() {
        return (
            <div style={{marginTop: 50}}>
               <h3>Add Product</h3>
               <Form onSubmit={this.onSubmit}>

                <FormGroup row>
                  <Label for="name" sm={2}>Product Name</Label>
                  <Col sm={10}>
                    <Input type="text" name="name" id="name" placeholder="Name of the Product" value={ this.state.name } onChange={ this.onChangeHostName } />
                  </Col>
                </FormGroup>
               <FormGroup row>
                 <Label for="brandName" sm={2}>Brand Name</Label>
                 <Col sm={10}>
                   <Input type="text" name="brandName" id="brandName" placeholder="Brand Name" value={ this.state.brandName } onChange={ this.onChangeHostName } />
                 </Col>
               </FormGroup>
               <FormGroup row>
                    <Label for="productType" sm={2}>Product Type</Label>
                    <Col sm={10}>
                      <Input type="text" name="productType" id="productType" placeholder="Product Type"  value={ this.state.productType } onChange={ this.onChangeHostName } />
                    </Col>
              </FormGroup>
              <FormGroup row>
                   <Label for="price" sm={2}>Price</Label>
                   <Col sm={10}>
                     <Input type="text" name="price" id="price" placeholder="Price"  value={ this.state.price } onChange={ this.onChangeHostName } />
                   </Col>
                 </FormGroup>
              <FormGroup row>
                   <Label for="size" sm={2}>Size/Dimension</Label>
                   <Col sm={10}>
                     <Input type="text" name="size" id="size" placeholder="Size"  value={ this.state.size } onChange={ this.onChangeHostName }  />
                   </Col>
                 </FormGroup>
              <FormGroup row>
                   <Label for="productColor" sm={2}>Product Color</Label>
                   <Col sm={10}>
                     <Input type="text" name="productColor" id="productColor" placeholder="Product Color"  value={ this.state.productColor } onChange={ this.onChangeHostName } />
                   </Col>
                 </FormGroup>
                <FormGroup check row>
                  <Col sm={{ size: 10, offset: 2 }}>
                    <Button>Submit</Button>
                  </Col>
                </FormGroup>
              </Form>
            </div>
        )
    }
}