import {observable, action} from 'mobx';

class Products{
    @observable all = [];
    @observable isLoading = false;

    @action async fetchAll(){
        this.isLoading = false;

        const response = await fetch('http://localhost:8080/rest/products/all');
        const status = await response.status;


        if(status === 200){
            this.all = await response.json();
        }

    }

    @action add(data){
        const headers = new Headers();
        headers.append('Content-Type', )
    }
}