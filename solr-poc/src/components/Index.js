import React, { Component } from 'react';
import { Col, Button, Form, FormGroup, Label, Input } from 'reactstrap';
import axios from 'axios';
// Import React Table
import ReactTable from "react-table";
import "react-table/react-table.css";

export default class Index extends Component {
    constructor(){
        super()
        this.state ={
            listing:[]
        }
    }

    componentWillMount(){
        axios.get('http://localhost:8080/rest/products/all').then(resp=>{console.log(resp);

            this.setState({listing:resp.data.content})
        }).catch(error=>{
            console.log(error);
        });
    }
    render(){
     const { listing } = this.state;
        return (
           <div>
               <ReactTable
                 data={listing}
                 columns={[
                   {
                     Header: "Product Details",
                     columns: [
                       {
                         Header: "Product Name",
                         accessor: "name"
                       },
                       {
                         Header: "Brand Name",
                         id: "brandName",
                         accessor: d => d.brandName
                       }
                     ]
                   },
                   {
                     Header: "Info",
                     columns: [
                       {
                         Header: "Type",
                         accessor: "productType"
                       },
                       {
                         Header: "Price",
                         accessor: "price"
                       },
                       {
                         Header: "Size",
                         accessor: "size"
                       },
                       {
                         Header: "Color",
                         accessor: "productColor"
                       }
                     ]
                   }
                 ]}
                 defaultPageSize={20}
                 style={{
                   height: "400px" // This will force the table body to overflow and scroll, since there is not enough room
                 }}
                 className="-striped -highlight"
               />
               <br />
             </div>
        )
    }
}
