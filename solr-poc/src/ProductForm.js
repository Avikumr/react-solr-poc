import React from 'react';
import { Col, Button, Form, FormGroup, Label, Input } from 'reactstrap';
import axios from 'axios';

export default class ProductForm extends React.Component {

    state = {
        products: []
      }

    componentDidMount(){
        axios.get('http://localhost:8080/rest/products/all')
          .then(res => {
            const products = res.data;
            this.setState({ products });
          })
    }

    handleSubmit = event => {
      event.preventDefault();

      const products = {
        products: this.state.products
      };

      axios.post('http://localhost:8080/rest/products/load', { products })
        .then(res => {
          console.log(res);
          console.log(res.data);
        })
    }



  render() {
    return (
      <Form onSubmit={this.handleSubmit}>
          <h3>Product Size</h3>
          Size: {this.state.products.length}
        <FormGroup row>
          <Label for="productName" sm={2}>Product Name</Label>
          <Col sm={10}>
            <Input type="text" name="productName" id="productName" placeholder="Name of the Product" />
          </Col>
        </FormGroup>
       <FormGroup row>
         <Label for="brandName" sm={2}>Brand Name</Label>
         <Col sm={10}>
           <Input type="text" name="brandName" id="brandName" placeholder="Brand Name" />
         </Col>
       </FormGroup>
       <FormGroup row>
            <Label for="productType" sm={2}>Product Type</Label>
            <Col sm={10}>
              <Input type="text" name="productType" id="productType" placeholder="Product Type" />
            </Col>
      </FormGroup>
      <FormGroup row>
           <Label for="price" sm={2}>Price</Label>
           <Col sm={10}>
             <Input type="text" name="price" id="price" placeholder="Price" />
           </Col>
         </FormGroup>
      <FormGroup row>
           <Label for="size" sm={2}>Size/Dimension</Label>
           <Col sm={10}>
             <Input type="text" name="size" id="size" placeholder="Size" />
           </Col>
         </FormGroup>
      <FormGroup row>
           <Label for="productColor" sm={2}>Product Color</Label>
           <Col sm={10}>
             <Input type="text" name="productColor" id="productColor" placeholder="Product Color" />
           </Col>
         </FormGroup>
        <FormGroup check row>
          <Col sm={{ size: 10, offset: 2 }}>
            <Button>Submit</Button>
          </Col>
        </FormGroup>
      </Form>


    );
  }
}