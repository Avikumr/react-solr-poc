import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import Create from './components/Create';
import Index from './components/Index';
import './App.css';

class App extends Component {
  render() {
    return (
    <Router>
      <div className="container">
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
          <a className="navbar-brand"><Link to={'/'} className="nav-link">Product Details</Link></a>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav mr-auto">
              <li className="nav-item"><a className="navbar-brand"><Link to={'/create'} className="nav-link">Create</Link></a></li>
            </ul>
            <hr />
          </div>
        </nav> <br />
        <Switch>
            <Route exact path='/create' component={ Create } />
            <Route exact path='/' component={ Index } />
        </Switch>

      </div>
    </Router>
    );
  }
}

export default App;
